package com.jaradlittle.mycheckins.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaradlittle.mycheckins.R;
import com.jaradlittle.mycheckins.database.Record;

import java.util.ArrayList;

/**
 * Created by Jarad Little on 30/08/2019.
 */
public class RecordRecyclerAdapter extends RecyclerView.Adapter<RecordRecyclerAdapter.RecordViewHolder> {

    private ArrayList<Record> mRecords;

    public RecordRecyclerAdapter(ArrayList<Record> records) {
        mRecords = records;
    }

    @NonNull
    @Override
    public RecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_record, parent, false);

        return new RecordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordViewHolder recordViewHolder, int position) {
        Record record = mRecords.get(position);
        recordViewHolder.title.setText(record.getTitle());
        recordViewHolder.place.setText(record.getPlace());
        // TODO: Add date conversion here
        recordViewHolder.date.setText(record.getTitle());
    }

    @Override
    public int getItemCount() {
        return mRecords.size();
    }

    class RecordViewHolder extends RecyclerView.ViewHolder {
        TextView title, place, date;

        RecordViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_title);
            place = itemView.findViewById(R.id.item_place);
            date = itemView.findViewById(R.id.item_date);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
