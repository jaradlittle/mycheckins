package com.jaradlittle.mycheckins.ui;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaradlittle.mycheckins.R;
import com.jaradlittle.mycheckins.database.Record;
import com.jaradlittle.mycheckins.database.RecordBaseHelper;
import com.jaradlittle.mycheckins.database.RecordSchema;
import com.jaradlittle.mycheckins.util.DateUtils;

/**
 *
 */
public class ViewRecordFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Record mRecord;
    private RecordBaseHelper mRecordBaseHelper;
    private SQLiteDatabase db;

    public ViewRecordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViewRecordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewRecordFragment newInstance(String param1, String param2) {
        ViewRecordFragment fragment = new ViewRecordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecordBaseHelper = new RecordBaseHelper(getActivity());
        db = mRecordBaseHelper.getWritableDatabase();

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mRecord = mRecordBaseHelper.getRecord(getArguments().getInt("recordId", -1));
        }

    }

    private EditText mTitleET, mPlaceET, mDetailsET;
    private Button mSetDateBtn, mShowMapBtn, mTakePhotoBtn, mShareBtn;
    private TextView mLatLngTV;
    private ImageView mPhotoImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_record, container, false);

        // Initialise the views.
        mTitleET = rootView.findViewById(R.id.record_title);
        mPlaceET = rootView.findViewById(R.id.record_place);
        mDetailsET = rootView.findViewById(R.id.record_details);
        mSetDateBtn = rootView.findViewById(R.id.set_date_btn);
        mLatLngTV = rootView.findViewById(R.id.lat_long_textview);
        mShowMapBtn = rootView.findViewById(R.id.show_map_btn);
        mPhotoImageView = rootView.findViewById(R.id.photo_imgview);
        mTakePhotoBtn = rootView.findViewById(R.id.take_photo_btn);
        mShareBtn = rootView.findViewById(R.id.share_btn);

        // Set the edittext values and make them uneditable
        mTitleET.setText(mRecord.getTitle());
        mTitleET.setEnabled(false);
        mPlaceET.setText(mRecord.getPlace());
        mPlaceET.setEnabled(false);
        mDetailsET.setText(mRecord.getDetails());
        mDetailsET.setEnabled(false);

        mSetDateBtn.setText(DateUtils.getStringFromDate(mRecord.getDate()));
        mSetDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        String location = mRecord.getLatitude() + ", \n" + mRecord.getLongitude();
        mLatLngTV.setText(location);
        mShowMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mPhotoImageView.setImageBitmap(mRecord.getBitmap());
        mTakePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO copy share function from other fragment.
            }
        });

        Button deleteBtn = rootView.findViewById(R.id.delete_btn);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.delete(RecordSchema.RecordTable.NAME,
                        RecordSchema.RecordTable.Cols.UUID + "=" + mRecord.getId(),
                        null);
            }
        });

        return rootView;
    }
}
