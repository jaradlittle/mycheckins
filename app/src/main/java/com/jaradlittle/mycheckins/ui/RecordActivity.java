package com.jaradlittle.mycheckins.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jaradlittle.mycheckins.R;

/**
 * Created by Jarad Little on 3/09/2019.
 */
public class RecordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        boolean newRecord = getIntent().getBooleanExtra("newRecord", false);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (newRecord)
            fragmentTransaction.replace(R.id.content, new NewRecordFragment());
        else
            fragmentTransaction.replace(R.id.content, new ViewRecordFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
