package com.jaradlittle.mycheckins.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaradlittle.mycheckins.R;
import com.jaradlittle.mycheckins.database.Record;
import com.jaradlittle.mycheckins.database.RecordBaseHelper;
import com.jaradlittle.mycheckins.database.RecordSchema;
import com.jaradlittle.mycheckins.util.DateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jarad Little on 31/08/2019.
 */
public class NewRecordFragment extends Fragment {

    private boolean readOnly = false;
    private boolean permissionGranted = false;
    private RecordBaseHelper mRecordBaseHelper;
    private SQLiteDatabase db;
    private Record mRecord;

    public static NewRecordFragment newInstance(boolean isReadOnly) {
        NewRecordFragment fragment = new NewRecordFragment();

        Bundle args = new Bundle();
        args.putBoolean("readOnly", isReadOnly);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecordBaseHelper = new RecordBaseHelper(getActivity());
        db = mRecordBaseHelper.getWritableDatabase();

        // Set fragment to read only if not creating new record.
        if (getArguments() != null) {
            mRecord = mRecordBaseHelper.getRecord(getArguments().getInt("recordId", -1));
        }
    }

    private EditText mTitleET, mPlaceET, mDetailsET;
    private Button mSetDateBtn, mShowMapBtn, mTakePhotoBtn, mShareBtn;
    private TextView mLatLngTV;
    private ImageView mPhotoImageView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_record, container, false);
        // Initialise the views.
        mTitleET = rootView.findViewById(R.id.record_title);
        mPlaceET = rootView.findViewById(R.id.record_place);
        mDetailsET = rootView.findViewById(R.id.record_details);
        mSetDateBtn = rootView.findViewById(R.id.set_date_btn);
        mLatLngTV = rootView.findViewById(R.id.lat_long_textview);
        mShowMapBtn = rootView.findViewById(R.id.show_map_btn);
        mPhotoImageView = rootView.findViewById(R.id.photo_imgview);
        mTakePhotoBtn = rootView.findViewById(R.id.take_photo_btn);
        mShareBtn = rootView.findViewById(R.id.share_btn);

        // Populate views with default data if new record.
        // Put the focus on the first edittext
        mTitleET.requestFocus();
        // Get the current date
        Date currentDate = Calendar.getInstance().getTime();
        // Set the date button with the current date.
        mSetDateBtn.setText(DateUtils.getStringFromDate(currentDate));
        // Show date picker on date button click
        mSetDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Show date picker
            }
        });

        // Check if app has necessary permissions
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Request permissions
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1337);
        }
        if (permissionGranted) {
            getLocation(mLatLngTV);
        } else {
            mLatLngTV.setText("Unable to access location.");
        }

        // Show map with current coordinates on show map button clicked
        mShowMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: show map with lat and long
            }
        });

        // Show camera view when take photo clicked
        mTakePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: show camera view.
            }
        });

        // Share to contact when share button clicked
        mShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Update this to proper information
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Here is the share content body";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        // Save the record to the database.
        // Get the values from the fields.
        String title = mTitleET.getText().toString();
        String place = mPlaceET.getText().toString();
        String details = mDetailsET.getText().toString();
        String latLng = mLatLngTV.getText().toString();
        // Split the coords into latitude and longitude.
        String[] coords = latLng.split(",");
        // Trim any whitespace from new strings and convert them into floats for storage.
        float latitude = Float.valueOf(coords[0].trim());
        float longitude = Float.valueOf(coords[1].trim());
        // Convert the string in the date button into a date object.
        Date date = DateUtils.getDateFromString(mSetDateBtn.getText().toString());
        // Retrieve the photo from the imageview.
        Bitmap photo = ((BitmapDrawable) mPhotoImageView.getDrawable()).getBitmap();
        Record record = new Record(title, place, details, latitude, longitude, date, photo);
        mRecordBaseHelper.createRecord(record);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 1337) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionGranted = true;
            } else {
                // Permission denied
                // TODO: show a dialog
            }
        }
    }

    private void getLocation(TextView targetView) {
        // Get current lat and long
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // We have already checked for permission beforehand so suppress unnecessary warning.
        @SuppressLint("MissingPermission")
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        // Put the values into textview
        targetView.setText(String.format("Latitude: %s%sLongitude: %s", latitude,
                System.getProperty("line.separator"), longitude));
    }
}
