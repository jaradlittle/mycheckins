package com.jaradlittle.mycheckins.database;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by Jarad Little on 30/08/2019.
 */
public class Record {
    private int mId;
    private String mTitle, mPlace, mDetails;
    private float mLatitude, mLongitude;
    private Date mDate;
    private Bitmap mBitmap;

    /**
     * Basic constructor for display in RecordRecyclerAdapter.
     */
    public Record(int id, String title, String place, Date date) {
        mId = id;
        mTitle = title;
        mPlace = place;
        mDate = date;
    }

    public Record(String title, String place, String details, float latitude, float longitude, Date date, Bitmap bitmap) {
        mTitle = title;
        mPlace = place;
        mDetails = details;
        mLatitude = latitude;
        mLongitude = longitude;
        mDate = date;
        mBitmap = bitmap;
    }

    /**
     * // TODO fix params here.
     * Complete constructor for detailed display and database transactions.
     * @param title The title of the record.
     * @param  place The place of the record.
     * @param details Any details that need to be recorded.
     * @param latitude GPS coordinates (Latitude, Longitude)
     * @param date
     * @param bitmap
     */
    public Record(int id, String title, String place, String details, float latitude,
                  float longitude, Date date, Bitmap bitmap) {
        mId = id;
        mTitle = title;
        mPlace = place;
        mDetails = details;
        mLatitude = latitude;
        mLongitude = longitude;
        mDate = date;
        mBitmap = bitmap;
    }

    /**
     * Empty constructor required for database retrieval.
     */
    Record() {

    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String place) {
        mPlace = place;
    }

    public String getDetails() {
        return mDetails;
    }

    public void setDetails(String details) {
        mDetails = details;
    }

    public float getLatitude() {
        return mLatitude;
    }

    public void setLatitude(float latitude) {
        mLatitude = latitude;
    }

    public float getLongitude() {
        return mLongitude;
    }

    public void setLongitude(float longitude) {
        mLongitude = longitude;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }
}
