package com.jaradlittle.mycheckins.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.jaradlittle.mycheckins.util.DateUtils;

import java.util.ArrayList;

/**
 * Created by Jarad Little on 11/09/2019.
 */
public class RecordBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "records.db";

    public RecordBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + RecordSchema.RecordTable.NAME + "("
                + RecordSchema.RecordTable.Cols.UUID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + RecordSchema.RecordTable.Cols.TITLE + " VARCHAR(50) NOT NULL,"
                + RecordSchema.RecordTable.Cols.PLACE + " VARCHAR(150) NOT NULL,"
                + RecordSchema.RecordTable.Cols.DETAILS + " TEXT,"
                + RecordSchema.RecordTable.Cols.LATITUDE + " DECIMAL NOT NULL,"
                + RecordSchema.RecordTable.Cols.LONGITUDE + " DECIMAL NOT NULL,"
                + RecordSchema.RecordTable.Cols.DATE + " DATETIME NOT NULL,"
                + RecordSchema.RecordTable.Cols.PHOTO + " BLOB);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void createRecord(Record record) {

    }

    /**
     * Retrieve all records in the database.
     */
    public ArrayList<Record> getAllRecords() {
        ArrayList<Record> records = new ArrayList<Record>();
        String selectQuery = "SELECT  * FROM " + RecordSchema.RecordTable.NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Record record = new Record();
                record.setId(c.getInt(c.getColumnIndex(RecordSchema.RecordTable.Cols.UUID)));
                record.setTitle(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.TITLE)));
                record.setPlace(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.PLACE)));
                record.setDetails(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.DETAILS)));
                record.setLatitude(c.getFloat(c.getColumnIndex(RecordSchema.RecordTable.Cols.LATITUDE)));
                record.setLongitude(c.getFloat(c.getColumnIndex(RecordSchema.RecordTable.Cols.LONGITUDE)));
                record.setDate(DateUtils.getDateFromString(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.DATE))));
                byte[] imageBytes = c.getBlob(c.getColumnIndex(RecordSchema.RecordTable.Cols.PHOTO));
                Bitmap photo =  BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                record.setBitmap(photo);

                // adding to todo list
                records.add(record);
            } while (c.moveToNext());
        }

        c.close();
        return records;
    }

    /**
     * Retrieve a record from the db.
     * @param id The id of the record in the db.
     */
    public Record getRecord(int id) {
        // Check if a legitimate request has been made.
        if (id == -1) {
            return null;
        }
        // Record should exist in database.
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + RecordSchema.RecordTable.NAME + " WHERE "
                + RecordSchema.RecordTable.Cols.UUID + " = " + id;
        Cursor c = db.rawQuery(selectQuery, null);
        Record record = new Record();

        if (c != null) {
            c.moveToFirst();
            record.setId(c.getInt(c.getColumnIndex(RecordSchema.RecordTable.Cols.UUID)));
            record.setTitle(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.TITLE)));
            record.setPlace(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.PLACE)));
            record.setDetails(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.DETAILS)));
            record.setLatitude(c.getFloat(c.getColumnIndex(RecordSchema.RecordTable.Cols.LATITUDE)));
            record.setLongitude(c.getFloat(c.getColumnIndex(RecordSchema.RecordTable.Cols.LONGITUDE)));
            record.setDate(DateUtils.getDateFromString(c.getString(c.getColumnIndex(RecordSchema.RecordTable.Cols.DATE))));
            byte[] imageBytes = c.getBlob(c.getColumnIndex(RecordSchema.RecordTable.Cols.PHOTO));
            Bitmap photo =  BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            record.setBitmap(photo);
            c.close();
        }
        return record;
    }

    /**
     * Delete record from the database.
     *
     * @param record The record to be deleted.
     */
    public void deleteRecord(Record record) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + RecordSchema.RecordTable.NAME + " WHERE id = '" + record.getId()
                + "'");
    }
}
