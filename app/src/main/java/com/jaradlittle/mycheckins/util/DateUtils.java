package com.jaradlittle.mycheckins.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jarad Little on 4/09/2019.
 */
public class DateUtils {

    /**
     * Converts a string into a date object.
     *
     * @param dateString The string to be converted.
     */
    public static Date getDateFromString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date date;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
        return date;
    }

    /**
     * Converts a date into a string.
     *
     * @param date The date to be converted.
     */
    public static String getStringFromDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return sdf.format(date);
    }
}
