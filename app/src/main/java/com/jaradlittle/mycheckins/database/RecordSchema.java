package com.jaradlittle.mycheckins.database;

/**
 * Created by Jarad Little on 11/09/2019.
 */
public class RecordSchema {
    public static final class RecordTable {
        public static final String NAME = "records";

        public static final class Cols {
            public static final String UUID = "id";
            public static final String TITLE = "title";
            public static final String PLACE = "place";
            public static final String DETAILS = "details";
            public static final String LATITUDE = "latitude";
            public static final String LONGITUDE = "longitude";
            public static final String DATE = "date";
            public static final String PHOTO = "photo";
        }
    }
}
