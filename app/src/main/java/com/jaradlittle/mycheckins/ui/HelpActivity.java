package com.jaradlittle.mycheckins.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jaradlittle.mycheckins.R;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        WebView mWebView = findViewById(R.id.help_webview);
        //TODO: Check the webview section to see if this is up to standard
        mWebView.getSettings().setJavaScriptEnabled(false);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl("https://www.wikihow.com/Check-In-on-Facebook");
    }
}
