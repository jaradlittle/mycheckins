package com.jaradlittle.mycheckins.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaradlittle.mycheckins.R;
import com.jaradlittle.mycheckins.adapter.RecordRecyclerAdapter;
import com.jaradlittle.mycheckins.database.Record;
import com.jaradlittle.mycheckins.database.RecordBaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Jarad Little on 30/08/2019.
 */
public class ListUIFragment extends Fragment {

    private RecyclerView mRecordsRecyclerView;
    private ArrayList<Record> mRecords;
    private RecordRecyclerAdapter mAdapter;
    private TextView mNoRecordsTextView;

    private RecordBaseHelper mRecordBaseHelper;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_ui, container, false);
        mRecordBaseHelper = new RecordBaseHelper(getActivity());
        mRecords = retrieveAllRecords();
        mAdapter = new RecordRecyclerAdapter(mRecords);

        mRecordsRecyclerView = rootView.findViewById(R.id.records_recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecordsRecyclerView.setLayoutManager(mLayoutManager);
        mRecordsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecordsRecyclerView.setAdapter(mAdapter);
        mNoRecordsTextView = rootView.findViewById(R.id.no_records_textview);
        if (mRecords.size() > 0) {
            mNoRecordsTextView.setVisibility(View.GONE);
            mRecordsRecyclerView.setVisibility(View.VISIBLE);
        }
        mAdapter.notifyDataSetChanged();
        return rootView;
    }

    private ArrayList<Record> retrieveAllRecords() {
        return mRecordBaseHelper.getAllRecords();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Update the list when returning from the NewRecordFragment in case a record has been created,
        // updated, or deleted.
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }
}
